To build an ONICS debian package do the following:

  # Clone this repo
  $ git clone /path/to/onics-deb
  $ cd onics-deb

  # Clone catlib
  $ git clone https://gitlab.com/catlib/catlib.git catlib

  # Clone onics 
  $ git clone https://gitlab.com/onics/onics.git onics

  # Set up debian build
  $ ./odeb.sh onics catlib

  # Perform the debian build
  $ (cd build/onics-1.0 ; dpkg-buildpackage -us -uc)

  # The debian package is in build/*.deb
