#!/bin/sh

REL=1.0
SANDBOX=build
PKGDIR=$SANDBOX/onics-${REL}
CATLIBDIR=$PKGDIR/catlib
DEBDIR=$PKGDIR/debian

[ $# -lt 2 ] && echo "usage: $0 /path/to/onics /path/to/catlib" >&2 && exit 1


echo Cleaning old directory >&2
rm -rf $PKGDIR
echo Creating $PKGDIR >&2
mkdir -p $PKGDIR
mkdir $CATLIBDIR
mkdir $DEBDIR
echo Cleaning ONICS and catlib for sanity >&2
make -C $1 veryclean
make -C $2 clean
echo Copying files from repos >&2
cp -r $1/* $PKGDIR
cp -r $2/build $2/conf $2/include $2/lib $2/LICENSE.txt \
      $2/README.txt $2/src/ $CATLIBDIR
echo Adding debian files >&2
cp rules $DEBDIR/rules
cp changelog $DEBDIR/changelog
cp compat $DEBDIR/compat
cp control $DEBDIR/control
cp copyright $DEBDIR/copyright
